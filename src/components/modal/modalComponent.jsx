import React from 'react';
import Candidate from '../candidates/candidate.jsx';

class ModalComponent extends React.Component {
  constructor(props) {
    super(props);
    this._closeModal = this._closeModal.bind(this);
  }
  
  _closeModal() {
    this.props.closeModal();
  }
  
  render() {
    return (
      <div className='c-modal'>
        <div className='c-modal__overlay'></div>
        <div className='c-modal__body'>
          <div className='c-modal__body__close' onClick={this._closeModal}>
            <span><i className='fa fa-times'></i></span>
          </div>
          <Candidate {...this.props} noCursor={true} />
        </div>
      </div>
    );
  }
}

export default ModalComponent;