import React from 'react';
import { connect } from 'react-redux';
import Header from './header/header.jsx';
import FiltersListContainer from './filters/filtersListContainer.jsx';
import CandidatesContainer from './candidates/candidatesContainer.jsx';
import ModalComponent from './modal/modalComponent.jsx';
import { updateFilters } from '../actions/filterActions.js';
import { fetchMetaList } from '../actions/masterDataActions.js';
import { getDiff } from '../helpers/utilHelpers.js';
import { fetchCandidates } from '../actions/candidatesActions.js';
import { removeModal } from '../actions/modalActions.js';

class App extends React.Component {
  constructor() {
    super()
  }
  
  componentWillMount() {
    // this.props.updateFilterState(this.props.location.query);
    // this.props.fetchMetaList();
    fetchCandidates();
  }
  
  componentWillReceiveProps(nextProps) {
    if(nextProps.location.search != this.props.location.search) {
      this.props.updateFilterState(nextProps.location.query);
    }
  }
  
  render() {
    return (
      <div>
        <Header {...this.props}/>
        <CandidatesContainer />
        {this.props.modal.active?(<ModalComponent {...this.props.modal.candidate} noBorder={true} closeModal={this.props.closeModal}/>):''}
      </div>
    );
  }
}

export default connect((state) => ({
  filters: state.filters,
  modal: state.modal
}), (dispatch) => {
  return {
    fetchMetaList() {
      dispatch(fetchMetaList());
    },
    closeModal() {
      dispatch(removeModal());
    }
  }
})(App);