import React from 'react';
import { connect } from 'react-redux';
import { searchCandidates } from '../../actions/searchActions.js';
import { openModal } from '../../actions/modalActions.js';
import SearchList from '../searchList.jsx';

const mapStateToProps = function(state) {
  return {
    ...state.search,
    placeholder: 'Search by name',
    list: state.search.list,
    avatar: true,
    icon: 'search',
    label: '',
    loading: state.search.loading,
    offset: state.search.offset,
    
  };
}

const mapDispatchToProps = function(dispatch) {
  return {
    keyUpHandler(query) {
      console.log(query);
      dispatch(searchCandidates(false, query));
    },
    onScrollToBottom(query) {
      dispatch(searchCandidates(true, query));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchList);