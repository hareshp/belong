import React from 'react';

class SearchComponent extends React.Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <div className="c-search">
        <input type="text" className="o-input c-search__input" placeholder='Search By Name'/>
        <span className='c-search__search-icon'><i className="fa fa-search"></i></span>
      </div>
    );
  }
}

export default SearchComponent;