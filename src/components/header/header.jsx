import React from 'react';
import FilterStatusComponent from '../filters/filtersStatusComponent.jsx';
import SearchContainer from './searchContainer.jsx';
import FiltersListContainer from '../filters/filtersListContainer.jsx';

class Header extends React.Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return(
      <div className='c-header'>
        <div className='pure-g c-header__fixed'>
          <div className='pure-u-4-5'>
            <img src='../../../assets/images/adhyayan_logo.jpg' />
            <FilterStatusComponent {...this.props}/>
          </div>
          <div className='pure-u-1-5'>
            <SearchContainer />
          </div>
        </div>
      </div>
    );
  }
}

export default Header;