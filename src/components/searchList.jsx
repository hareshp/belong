import React from 'react';
import { findDOMNode } from 'react-dom';
import AvatarListItem from './avatarListItem.jsx';

class SearchList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: ""
    };
    this._keyPressed = this._keyPressed.bind(this);
    this._itemClicked = this._itemClicked.bind(this);
    this._onScroll = this._onScroll.bind(this);
  }
  
  componentDidMount() {
    if(this.refs.list) {
      findDOMNode(this.refs.list).addEventListener('scroll', this._onScroll);
    }
  }
  
  _onScroll(e) {
    if(this.props.onScrollToBottom) {
      if((!this.props.loading && this.props.offset !== -1) && (this.refs.list.scrollTop >= (this.refs.list.scrollHeight - this.refs.list.offsetHeight))) {
        this.props.onScrollToBottom(this.state.query);
      }
    }
  }
  
  _keyPressed(e) {
    this.setState({
      query: e.target.value
    });
    this.props.keyUpHandler(e.target.value);
  }
  
  _itemClicked(item) {
    this.props.onItemClick(item);
  }
  
  renderList() {
    if(this.props.avatar) {
      return(
        <ul className='c-search__list' ref='list'>
          {this.props.list.length?this.props.list.map((item) => (
            <li key = {item.uid} className='c-search__list__avatar-item' onMouseDown={this._itemClicked.bind(this, item)}>
              <AvatarListItem {...item} />
            </li>
          )):(<div className="c-search__no-results">The result list is empty.</div>)}
          {this.props.loading?(<li className='c-search__list__loading'><img src='/assets/images/loading.svg'/></li>):''}
        </ul>
      );
    } else {
      return (
        <ul className='c-search__list'>
          {this.props.list.map((item, index) => (
            <li key={index} className='c-search__list__item' onMouseDown={this._itemClicked.bind(this, item)}>
              {item}
            </li>
          ))}
        </ul>
      );
    }
  }
  
  render() {
    return (
      <div className="c-search">
        {this.props.label != ''?(<label className='c-search__label'>{this.props.label}</label>):''}
        <div className='c-search__input-wrapper'>
          <input type="text" className="o-input c-search__input" placeholder={this.props.placeholder} value={this.state.query} onChange={this._keyPressed} />
          {this.props.icon == 'search'?<span className='c-search__icon'><i className="fa fa-search"></i></span>
          :<span className='c-search__icon c-search__dropdown-icon'><i className='fa fa-angle-down'></i></span>}
          {/* {this.renderList()} */}
          <span className='c-search__pointer'></span>
        </div>
      </div>
    );
  }
}

export default SearchList;
