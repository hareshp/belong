import React from 'react';
import { connect } from 'react-redux';
import { addFilter } from '../../actions/filterActions.js';
import { filterLocations, filterCompanies } from '../../actions/masterDataActions.js'
import FiltersList from './filtersList.jsx';

const mapStateToProps = function(state) {
  return {
    locations: state.masterData.locations.list.filter((ele) => (ele.toLowerCase().includes(state.masterData.locations.filterQuery.toLowerCase()) && state.filters.filter((filter) => (filter.type == 'location' && filter.value == ele)).length == 0)),
    companies: state.masterData.companies.list.filter((ele) => (ele.toLowerCase().includes(state.masterData.companies.filterQuery.toLowerCase()) && state.filters.filter((filter) => (filter.type == 'company' && filter.value == ele)).length == 0))
  }
}

const mapDispatchToProps = function(dispatch, ownProps) {
  return {
    addFilter(type, item) {
      let queryObj = ownProps.location.query;
      switch(type) {
        case 'locations':
          queryObj.current_location = queryObj.current_location?`${queryObj.current_location},${item}`:item
          break;
        case 'companies':
          queryObj.current_company = queryObj.current_company?`${queryObj.current_company},${item}`:item
          break;
      }
      ownProps.router.push({
        pathname: '/',
        query: queryObj
      })
    },
    filterList(type, query) {
      switch(type) {
        case 'locations':
          dispatch(filterLocations(query));
          break;
        case 'companies':
          dispatch(filterCompanies(query));
          break;
      }
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FiltersList);
