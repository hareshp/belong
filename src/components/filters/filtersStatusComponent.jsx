import React from 'react';
import { connect } from 'react-redux';
import { removeFilter } from '../../actions/filterActions.js';
import FiltersView from './filtersView.jsx';

const mapStateToProps = function(state) {
  return {
    filters: state.filters
  };
}

const mapDispatchToProps = function(dispatch, ownProps) {
  return {
    removeFilter(filter) {
      let queryObj = ownProps.location.query;
      switch(filter.type) {
        case 'location':
          queryObj.current_location = queryObj.current_location.split(',').filter(ele => ele !== filter.value).join(',');
          break;
        case 'company':
          queryObj.current_company = queryObj.current_company.split(',').filter(ele => (ele !== filter.value)).join(',');
          break;
      }
      ownProps.router.push({
        pathname: '/',
        query: queryObj
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FiltersView);
