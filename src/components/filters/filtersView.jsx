import React from 'react';

class FiltersView extends React.Component {
  constructor(props) {
    super(props);
    this.removeFilter = this.removeFilter.bind(this);
  }
  
  removeFilter(filter) {
    this.props.removeFilter(filter);
  }
  
  render() {
    return(
      <div>
        <div className='u-margin-10--bottom'>
          <span className='u-text-font u-spacer-10--right'>IPL2020 Tweets Search</span>
          <span className='u-subtext-font--small'>Applied Filters ({this.props.filters.length})</span>
        </div>
        {this.props.filters.length?
          <div>
            <span className='u-text-font--small'>Showing Candidates from </span>
            {this.props.filters.map((filter, index) => (
              <div className='o-tag u-spacer' key={index}>
                <div className='o-tag__label'>{filter.value}</div>
                <div className='o-tag__icon' onClick={this.removeFilter.bind(this, filter)}>
                  <i className='fa fa-times'></i>
                </div>
              </div>
            ))}
          </div>:<div></div>
        }
      </div>
    );
  }
}

export default FiltersView;