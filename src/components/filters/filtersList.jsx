import React from 'react';
import SearchList from '../searchList.jsx';

class FiltersList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addFilter: false
    };
    this.showFilters = this.showFilters.bind(this);
  }
  
  showFilters() {
    this.setState({
      addFilter: true
    });
  }
  
  render() {
    if(this.state.addFilter) {
      return(
        <div className='c-header__filters pure-g'>
          <div className='pure-u-5-24'>
            <SearchList label = 'Current Company' placeholder = 'Add a filter' icon='dropdown' list = {this.props.companies} keyUpHandler={this.props.filterList.bind(this, 'companies')} onItemClick = {this.props.addFilter.bind(this, 'companies')}/>
          </div>
          <div className='pure-u-5-24'>
            <SearchList label = 'Current Location' placeholder = 'Add a filter' icon='dropdown' list = {this.props.locations} keyUpHandler={this.props.filterList.bind(this, 'locations')} onItemClick = {this.props.addFilter.bind(this, 'locations')}/>
          </div>
        </div>
      );
    } else {
      return(
        <div className='c-header__filters u-t-link'>
          <p onClick={this.showFilters}><span><i className='fa fa-plus'></i></span> Add a Filter</p>
        </div>
      );
    }
  }
}

export default FiltersList;