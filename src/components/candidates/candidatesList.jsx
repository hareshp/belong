import React from 'react';
import Candidate from './candidate.jsx';

class CandidatesList extends React.Component {
  constructor(props) {
    super(props);
    this.handleScroll = this.handleScroll.bind(this);
    this._candidateSelected = this._candidateSelected.bind(this);
  }
  
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    this.handleScroll();
  }
  
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }
  
  _candidateSelected(candidate) {
    this.props.selectCandidate(candidate);
  }
  
  handleScroll(e) {
    if((!this.props.loading && this.props.offset !== -1) && (window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.props.fetchCandidates();
    }
  }
  
  render() {
    console.log("lsit");
    console.log(this.props.list);
    return(
      <div className='c-candidates-wrapper'>
        <div className='u-subtext-font'>
          <p className='u-t-center'>Showing {this.props.list.length} from {this.props.totalCount}</p>
        </div>
        <div className='c-candidates-wrapper__list'>
          {this.props.list.map((item) => (
            <Candidate key={item.uid} {...item} onItemClick={this._candidateSelected.bind(this, item)}/>
          ))}
        </div>
        {this.props.loading?<p className='u-t-center'><img src='/assets/images/loading.svg' /></p>:''}
      </div>
    );
  }
}

export default CandidatesList;