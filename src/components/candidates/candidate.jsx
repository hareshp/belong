import React from 'react';
import { getMonthAndYear, getDiffFromToday } from '../../helpers/utilHelpers.js'

class Candidate extends React.Component {
  constructor(props) {
    super(props);
    this._cardClicked = this._cardClicked.bind(this);
  }
  
  _cardClicked() {
    this.props.onItemClick();
  }
  
  render() {
    const className = this.props.noBorder?'':'o-card o-card--separated';
    const candidateClassName =  this.props.noCursor?'c-candidate c-candidate--no-pointer':'c-candidate';
    // const presentCompany = this.props.experience.filter(ele => ele.is_current);
    return (
      <div className={className} onClick={this._cardClicked}>
        <div className='o-card__section'>
          <div className={candidateClassName} >
            <div className='c-candidate__avatar'>
              {/* <img src={this.props.profile_picture ? this.props.profile_picture : ''} /> */}
            </div>
            <div className='c-candidate__details-section'>
              <p className="c-candidate__details-section__header">{this.props.user_name}</p>
              <div className="u-text-font">{this.props.user_location} <span className='u-subtext-font'>at</span> {this.props.current_company}</div>
              {/* {presentCompany.length?(<div className="u-subtext-font u-subtext-font--italic">({getDiffFromToday(presentCompany[0].start)}, since {getMonthAndYear(presentCompany[0].start)})</div>):''} */}
              <p className="u-text-font">{this.props.text}</p>
              <div className="u-text-font"><span className="u-subtext-font">Worked at </span><span className="role">{this.props.current_location}</span></div>
              {/* <div className="u-subtext-font u-subtext-font--italic">({parseInt(this.props.total_experience/12)} years {this.props.total_experience%12} months total experience)</div> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Candidate;