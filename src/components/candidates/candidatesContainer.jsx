import React from 'react';
import { connect } from 'react-redux';
import { fetchCandidates } from '../../actions/candidatesActions.js';
import { openModal } from '../../actions/modalActions.js';
import CandidatesList from './candidatesList.jsx';

const mapStateToProps = function(state) {
  return {
    ...state.candidates
  };
}

const mapDispatchToProps = function(dispatch) {
  return {
    fetchCandidates() {
      dispatch(fetchCandidates(true));
    },
    selectCandidate(candidate) {
      dispatch(openModal(candidate));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CandidatesList);