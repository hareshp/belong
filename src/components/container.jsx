import React from 'react';

class Container extends React.Component {
    constructor() {
        super();
    }
    
    render() {
        return (
            <div className='app'>
                {this.props.children}
            </div>
        );
    }
}

export default Container;
