import React from 'react';

class AvatarListItem extends React.Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return(
      <div className='c-avatarItem pure-g'>
        <div className='pure-10-24'>
          <img src={this.props.profile_picture} className='c-avatarItem__img' />
        </div>
        <div className='pure-14-24'>
          <div className='u-text-font'>
            {this.props.first_name} {this.props.last_name}
          </div>
          <div className='u-text-font--small'>
            {this.props.current_role}
          </div>
          <div className='u-text-font--small'>
            {this.props.current_company}
          </div>
        </div>
      </div>
    );
  }
}

export default AvatarListItem;