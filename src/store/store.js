import { routerReducer as routing } from 'react-router-redux';
import { createStore, combineReducers } from 'redux';
import candidates from '../reducers/candidatesReducer.js';
import filters from '../reducers/filtersReducer.js';
import masterData from '../reducers/masterDataReducer.js';
import search from '../reducers/searchReducer.js';
import modal from '../reducers/modalReducer.js';

export const finalReducer = combineReducers({
  routing,
  candidates,
  filters,
  masterData,
  search,
  modal
});

const mainStore = createStore(finalReducer);

export default mainStore;
