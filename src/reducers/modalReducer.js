import initialState from '../constants/initialState.js';
import actions from '../constants/actions.js';

export default function modalReducer(state=initialState.modal, action) {
  switch(action.type) {
    case actions.OPEN_MODAL:
      return {
        ...state,
        active: true,
        candidate: action.data.candidate
      };
      break;
    case actions.REMOVE_MODAL:
      return {
        ...state,
        active: false
      };
      break;
    default: 
      return state;
  }
}