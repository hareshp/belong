import initialState from '../constants/initialState.js';
import actions from '../constants/actions.js';

export default function filtersReducer(state=initialState.filters, action) {
  switch(action.type) {
    case actions.REMOVE_FILTER:
      return state.filter((item) => (item.type !== action.data.filter.type || item.value !== action.data.filter.value))
      break;
    case actions.ADD_FILTER:
      return [
        ...state,
        action.data.filter
      ];
      break;
    case actions.UPDATE_FILTERS:
      return action.data.filters
      break;
    default: 
      return state;
  }
}