import initialState from '../constants/initialState.js';
import actions from '../constants/actions';

export default function candidatesReducer(state=initialState.candidates, action) {
  switch(action.type) {
    case actions.FETCH_CANDIDATES_REQUEST_SENT:
      return {
        ...state,
        loading: true
      }
      break;
    case actions.FETCH_CANDIDATES_SUCCESS:
      if(action.data.append) {
        return {
          ...state,
          list: [...state.list, ...action.data.list]
        };
      } else {
        return {
          ...state,
          list: [...action.data.list],
          offset: action.data.offset,
          totalCount: action.data.totalCount,
          loading: false
        };
      }
      break;
    default: 
      return state;
  }
}