import initialState from '../constants/initialState.js';
import actions from '../constants/actions.js';

export default function masterDataReducer(state=initialState.masterData, action) {
  switch(action.type) {
    case actions.FETCH_META_LIST_REQUEST_SENT:
      return {
        ...state,
        loading: true
      }
      break;
    case actions.FETCH_META_LIST_SUCCESS:
      return {
        companies: {
          list: action.data.companies,
          filterQuery: ""
        },
        locations: {
          list: action.data.locations,
          filterQuery: ""
        },
        loading: false
      }
      break;
    case actions.FILTER_LOCATION_LIST:
      return {
        ...state,
        locations: {
          list: state.locations.list,
          filterQuery: action.data.query
        }
      }
      break;
    case actions.FILTER_COMPANY_LIST: 
      return {
        ...state,
        companies: {
          list: state.companies.list,
          filterQuery: action.data.query
        }
      }
      break;
    case actions.FETCH_META_LIST_ERROR:
      return {
        ...state,
        loading: false
      }
      break;
    default:
      return state;
  }
}