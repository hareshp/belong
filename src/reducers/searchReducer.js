import initialState from '../constants/initialState.js';
import actions from '../constants/actions';

export default function searchReducer(state=initialState.search, action) {
  switch(action.type) {
    case actions.SEARCH_CANDIDATES_REQUEST_SENT:
      return {
        ...state,
        loading: true
      };
      break;
    case actions.SEARCH_CANDIDATES_SUCCESS:
      if(action.data.append) {
        return {
          ...state,
          list: [...state.list, ...action.data.list],
          offset: action.data.offset,
          loading: false
        };
      } else {
        return {
          ...state,
          list: [...action.data.list],
          offset: action.data.offset,
          loading: false
        };
      }
      break;
    default: 
      return state;
  }
}