import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Container from './components/container.jsx';
import App from './components/app.jsx';
export default (
  <Route path="/" component={Container}>
    <IndexRoute component={App} />
  </Route>
);
