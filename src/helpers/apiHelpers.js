import superagent from 'superagent-bluebird-promise';

export function getMetaList() {
  return superagent('/api/meta');
}

export function getCandidatesList(query) {
  let queryString = "";
  console.log("GET CANDIDATES LIST");
  console.log(query);
  if(query) {
    queryString += `query=${query}&`;
  }
  return superagent('/getall').query(queryString);
}