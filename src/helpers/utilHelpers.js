export function getDiff(updated, original) {
  if(!updated && !original) {
    return {
      added: [],
      removed: []
    }
  } else if(!updated) {
    return {
      added: [],
      removed: original.split(',')
    };
  } else if(!original) {
    return {
      added: updated.split(','),
      removed: []
    };
  }
  const originalArray = original.split(',');
  const updatedArray = updated.split(',');
  return {
    added: updatedArray.filter(item => originalArray.indexOf(item) == -1),
    removed: originalArray.filter(item => updatedArray.indexOf(item) == -1)
  };
}

export function getDiffFromToday(startDate) {
  const currDate = new Date();
  startDate = new Date(startDate);
  let diffMonths = (currDate.getTime() - startDate.getTime())/(1000 * 3600 * 24 * 30);
  return `${parseInt(diffMonths/12)} Years ${parseInt(diffMonths % 12)} Months`;
}

export function getMonthAndYear(startDate) {
  startDate = new Date(startDate);
  const month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
  return `${month[startDate.getMonth()]} ${startDate.getFullYear()}`;
}