import { getMetaList } from '../helpers/apiHelpers';
import actions from '../constants/actions.js';

export function fetchMetaListRequestSent() {
  return {
    type: actions.FETCH_META_LIST_REQUEST_SENT
  }
}

export function fetchMetaListSuccess(data) {
  return {
    type: actions.FETCH_META_LIST_SUCCESS,
    data: {
      companies: data.companies,
      locations: data.locations
    }
  }
}

export function fetchMetaListError(err) {
  return {
    type: actions.FETCH_META_LIST_ERROR,
    data: {
      err
    }
  }
}

export function filterLocations(query) {
  return {
    type: actions.FILTER_LOCATION_LIST,
    data: {
      query
    }
  }
}

export function filterCompanies(query) {
  return {
    type: actions.FILTER_COMPANY_LIST,
    data: {
      query
    }
  }
}

export function fetchMetaList() {
  return (dispatch) => {
    dispatch(fetchMetaListRequestSent());
    return getMetaList().then((data) => {
      dispatch(fetchMetaListSuccess(JSON.parse(data.text)));
    }).catch((err) => {
      dispatch(fetchMetaListError(err));
    })
  }
}
