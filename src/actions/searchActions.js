import { getCandidatesList } from '../helpers/apiHelpers';
import actions from '../constants/actions';

export function searchCandidatesRequestSent() {
  return {
    type: actions.SEARCH_CANDIDATES_REQUEST_SENT
  }
}

export function searchCandidatesSuccess(data, appendList) {
  return {
    type: actions.SEARCH_CANDIDATES_SUCCESS,
    data: {
      list: data.candidates,
      append: appendList,
      offset: data.next_offset
    }
  }
}

export function searchCandidatesError(err) {
  return {
    type: actions.SEARCH_CANDIDATES_ERROR,
    data: {
      err
    }
  }
}

export function fetchCandidatesSuccess(data, appendList) {
  return {
    type: actions.FETCH_CANDIDATES_SUCCESS,
    data: {
      list: data,
    }
  };
}

export function searchCandidates(currentOffset = false, query) {
  return function(dispatch, getState) {
    const state = getState();
    // let currentCompany = state.filters.filter((ele) => (ele.type == 'company'));
    // let currentLocation = state.filters.filter((ele) => (ele.type == 'location'));
    dispatch(searchCandidatesRequestSent());
    return getCandidatesList(query).then(function(data) {
      dispatch(fetchCandidatesSuccess(JSON.parse(data.text), currentOffset));
    }).catch((err) => {
      console.log(err);
      dispatch(searchCandidatesError(err))
    })
  }
}