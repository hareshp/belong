import actions from '../constants/actions';

export function removeFilter(filter) {
  return {
    type: actions.REMOVE_FILTER,
    data: {
      filter
    }
  };
}

export function addFilter(payload) {
  return {
    type: actions.ADD_FILTER,
    data: {
      filter: payload
    }
  }
}

export function updateFilters(payload) {
  return {
    type: actions.UPDATE_FILTERS,
    data: {
      filters: payload
    }
  }
}