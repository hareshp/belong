import actions from '../constants/actions.js';

export function openModal(candidate) {
  return {
    type: actions.OPEN_MODAL,
    data: {
      candidate
    }
  };
}

export function removeModal(candidate) {
  return {
    type: actions.REMOVE_MODAL
  };
}