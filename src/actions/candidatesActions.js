import { getCandidatesList } from '../helpers/apiHelpers';
import actions from '../constants/actions';

export function fetchCandidatesRequestSent() {
  return {
    type: actions.FETCH_CANDIDATES_REQUEST_SENT
  };
}

export function fetchCandidatesSuccess(data, appendList) {
  return {
    type: actions.FETCH_CANDIDATES_SUCCESS,
    data: {
      list: data,
    }
  };
}

export function fetchCandidatesError(err) {
  return {
    type: actions.FETCH_CANDIDATES_ERROR,
    data: {
      err
    }
  }
}

export function fetchCandidates(currentOffset = false) {
  return function(dispatch, getState) {
    const state = getState();
    // let currentCompany = state.filters.filter((ele) => (ele.type == 'company'));
    // let currentLocation = state.filters.filter((ele) => (ele.type == 'location'));
    dispatch(fetchCandidatesRequestSent());
    return getCandidatesList().then((data) => {
      dispatch(fetchCandidatesSuccess(JSON.parse(data.text), currentOffset));
    }).catch((err) => {
      console.log(err);
      dispatch(fetchCandidatesError(err));
    });
  }
}