const actions = {
  'REMOVE_FILTER' : 'REMOVE_FILTER',
  'SEARCH_CANDIDATES_REQUEST_SENT' : 'SEARCH_CANDIDATES_REQUEST_SENT',
  'SEARCH_CANDIDATES_SUCCESS' : 'SEARCH_CANDIDATES_SUCCESS',
  'SEARCH_CANDIDATES_ERROR' : 'SEARCH_CANDIDATES_ERROR',
  'ADD_FILTER': 'ADD_FILTER',
  'REMOVE_FILTER': 'REMOVE_FILTER',
  'UPDATE_FILTERS': 'UPDATE_FILTERS',
  'FETCH_META_LIST_REQUEST_SENT': 'FETCH_META_LIST_REQUEST_SENT',
  'FETCH_META_LIST_SUCCESS': 'FETCH_META_LIST_SUCCESS',
  'FETCH_META_LIST_ERROR': 'FETCH_META_LIST_ERROR',
  'FILTER_LOCATION_LIST': 'FILTER_LOCATION_LIST',
  'FILTER_COMPANY_LIST': 'FILTER_COMPANY_LIST',
  'FETCH_CANDIDATES_REQUEST_SENT': 'FETCH_CANDIDATES_REQUEST_SENT',
  'FETCH_CANDIDATES_SUCCESS': 'FETCH_CANDIDATES_SUCCESS',
  'FETCH_CANDIDATES_ERROR': 'FETCH_CANDIDATES_ERROR',
  'OPEN_MODAL': 'OPEN_MODAL',
  'REMOVE_MODAL': 'REMOVE_MODAL'
};

export default actions;