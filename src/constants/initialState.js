const initialState = {
  filters: [],
  candidates: {
    list: [],
    totalCount: 0,
    offset: 0,
    loading: false
  },
  search: {
    query: "",
    list: [],
    offset: 0,
    loading: false
  },
  masterData: {
    companies: {
      list: [],
      filterQuery: ""
    },
    locations: {
      list: [],
      filterQuery: ""
    },
    loading: false
  },
  modal: {
    open: false,
    candidate: {}
  }
};

export default initialState;
