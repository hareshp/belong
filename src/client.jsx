import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { createStore, applyMiddleware,compose } from 'redux';
import thunk from 'redux-thunk';
import routes from './routes.jsx';
import { finalReducer } from './store/store.js';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const myStore = createStore(finalReducer, window.__INITIAL_STATE__, composeEnhancers(applyMiddleware(thunk)));

const history = browserHistory;
syncHistoryWithStore(history, myStore);

ReactDOM.render((
  <Provider store={myStore}>
    <Router history={history} onUpdate={() => window.scrollTo(0, 0)}>
      { routes }
    </Router>
  </Provider>
), document.getElementById('mainContainer'));
