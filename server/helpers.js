export function getAssetUrl(assetName) {
  let manifest;
  try{
    manifest = require('../../build/rev-manifest');
    return manifest[assetName] || assetName;
  } catch(e){
    return assetName;
  }
}