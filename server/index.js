require("babel-register");

var compress
  ,express
  ,path
  ,config
  ,render
  ,apiProxy
  ,errorHandler;

config = require('../config');

express           = require('express');
path              = require('path');
config            = require('../config').default;
render            = require('./middleware/render').default;
apiProxy          = require("./middleware/proxy").default;

const { Client } = require('pg');
var connectionString = "postgres://postgres:Adhyayan2020@35.200.190.89:5432/ipl2020_tweets";
const client = new Client({
    connectionString: connectionString
})

const app = express();

app.use('/assets', express.static(path.join(__dirname, '..', 'build')));


// API Proxy middleware
app.use('/api', apiProxy);

client.connect(err => {
  if (err) {
    console.error('connection error', err.stack)
  } else {
    console.log('connected')
  }
})

app.get('/getall', function (req, res, next) {
  console.log("INSIDE GETALL");
  console.log(req.query.query);
  let psqlquery = '';
  if(req.query.query){
    psqlquery = 'SELECT * FROM public.tweets where text like \'%'+req.query.query+'\' LIMIT 50'
  } else {
    psqlquery = 'SELECT * FROM public.tweets LIMIT 50' 
  }
  client.query(psqlquery).then(result => {
    console.log()
    res.json(result.rows);
  }).catch(err => {
    console.log(err);
  });
});

// For Home page
app.get('/', render);

const port = process.env.PORT || config.server_port;
app.listen(port, () => console.log(`Listening on, ${port}`));
