import proxy from 'http-proxy-middleware';
import config from '../../config';

// proxy middleware options
// https://www.npmjs.com/package/http-proxy-middleware#http-proxy-middleware-options
console.log(config.api_server.domain);
const options = {
  target: 'http://localhost',
  changeOrigin: true,
  pathRewrite: {
    '^/api': '/'
  },
  router: {
    "/": config.api_server.domain
  }
};

// create the proxy (without context)
const apiProxy = proxy(options);
export default apiProxy;