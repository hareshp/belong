import initialState from '../../src/constants/initialState';
import { getAssetUrl } from '../helpers';

let mainSrc = getAssetUrl('javascripts/build.js');

let cssSrc = getAssetUrl('stylesheets/main.css');

let vendorsSrc = getAssetUrl('javascripts/vendors.js');

const renderPage = function(req, res, next) {
    
    res.send(`
    <html lang="en-us">
      <head>
        <meta charset="utf-8">
        <title>Belong</title>
        <meta name="mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" type="text/css" href="/assets/${cssSrc}"/>
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <script>
            window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
        </script>
      </head>
      <body>
        <div id="mainContainer">
        </div>
        <script type="text/javascript" src="/assets/${vendorsSrc}"></script>
        <script type="text/javascript" src="/assets/${mainSrc}"></script>
      </body>
    </html>
    `);
}

export default renderPage