var path = require('path');
var webpack = require('webpack');
var CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports =  function(){
  var webpack_config = {
    entry: {
      build: [
        path.join(__dirname, '/src/client.jsx')
      ],
      vendors: [
        'react',
        'react-dom',
        'superagent',
        'react-redux',
        'react-router',
        'react-router-redux',
        'react-helmet',
        'lodash'
      ]
    },
    output: {
      filename:  "/javascripts/[name].js",
      path: "build"
    },
    node: {
      fs: "empty",
      net: "empty",
      tls: "empty"
    },
    stats: { children: false },
    module: {
      loaders : [
        {
          test: /(\.js$|\.jsx$)/,
          exclude: /node_modules/,
          loader: "babel",
          query: {
            cacheDirectory: true
          }
        },
        { test: /\.json$/,
          exclude: /node_modules/,
          loader: 'json-loader'
        }
      ]
    },
    plugins: [
      new webpack.optimize.CommonsChunkPlugin('vendors', '/javascripts/vendors.js', Infinity),
      new CleanWebpackPlugin(['build'], {
        verbose: true,
        dry: false,
        root: path.join(__dirname, './')
      })
    ]
  }
  webpack_config.devtool = "sourcemap";

  return webpack_config;
}