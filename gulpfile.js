"use strict";

var path         = require('path');
var gulp         = require('gulp-help')(require('gulp'), {aliases: ["ls"]});
var runSequence  = require("run-sequence");
var plugins      = require('gulp-load-plugins')({
  rename: {
    'gulp-rev-css-url': 'revCssUrl',
    'gulp-rev-replace': 'revReplace'
  }
});

var STATIC_ASSETS_PATH = [
  path.join(__dirname, "/assets/**/*"),
  "!" + path.join(__dirname, "/assets/stylesheets"),
  "!" + path.join(__dirname, "/assets/stylesheets/**/*")
];

function runTask(task) {
  return function(evt){
    console.log("\n ***********  RUNNING TASK: " + task + "  ***********");
    plugins.util.log('File', plugins.util.colors.cyan(evt.path), 'was ', plugins.util.colors.magenta("modified."));
    return runSequence(task);
  };
}

gulp.task("mv:assets", function(){
  return gulp.src(STATIC_ASSETS_PATH)
  .pipe(gulp.dest(path.join(__dirname, '/build')));
});

function getTask(task) {
  return require('./tasks/' + task)(gulp, plugins);
}



gulp.task('watch', function(cb){

  console.log("\n ............... Watching files for build ............... \n");

  gulp.watch([path.join(__dirname, '/src/**/*')], runTask('build-scripts'))
  gulp.watch([path.join(__dirname, '/assets/stylesheets/**/*.scss')], runTask('build-css'))
  gulp.watch(STATIC_ASSETS_PATH, runTask('mv:assets'))

  cb();
});

gulp.task('build-watch', function(cb){
  runSequence(
    "build",
    "watch",
    cb
  );
})

gulp.task('revision', getTask('revision')['rev:assets'])

gulp.task('build', function(cb){
  runSequence(
    'build-scripts',
    'build-css',
    'mv:assets',
    'revision',
    cb
    );
});

gulp.task('build-scripts', getTask('build').build);

gulp.task('build-css', getTask('build-css')());


