## Installation

 - Please run `npm run setup` for installing all the dependencies.
 
 - After the setup is done run `npm run start` which will build and run the server
 
 - Now the Server is running on port 3000, Go to the browser `localhost:3000` for the application.
