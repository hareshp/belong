const config = {
    name: 'Belong',
    api_server: {
        domain: 'http://localhost',
    },
    server_port: 3000,
}

export default config;
