var path = require('path');

module.exports = function (gulp, plugins) {

  var source_path = path.join(__dirname, "../", "/assets/stylesheets/*.scss");

  return function () {

    var destination_path = path.join(__dirname, "../build/stylesheets");

    console.log("\n ..............Compiling Stylesheets..............");
    console.log("Src: ",  source_path)
    console.log("Dest: ",  destination_path);

    return gulp.src(source_path)
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.sass({outputStyle: 'compressed'}).on('error', plugins.sass.logError))
      .pipe(plugins.autoprefixer('last 2 versions'))
      .pipe(plugins.bytediff.start())
      .pipe(plugins.cssnano())
      .pipe(plugins.bytediff.stop())
      .pipe(gulp.dest(destination_path))
  };

};
