var path         = require('path');
var chalk        = require("chalk");

module.exports = function(gulp, plugins) {
  
  var build_folder = path.join(__dirname, "../build");

  return {
    "rev:assets": function(){

      console.log(chalk.green("...................... Revisioning all assets .................."));

      return gulp.src(build_folder + "/**/*")
              .pipe(plugins.rev())
              .pipe(plugins.revCssUrl())
              .pipe(gulp.dest(build_folder))
              .pipe(plugins.rev.manifest())
              .pipe(gulp.dest(build_folder));

    },

    "rev:replace": function(){
      return gulp.src(build_folder + "/**/*")
        .pipe(plugins.revReplace({
          manifest: manifest
        }))
        .pipe(gulp.dest(build_folder));
    }
  }
}
