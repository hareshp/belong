var webpack = require("webpack");
var chalk   = require("chalk");

module.exports = function (gulp, plugins, config) {
  var webpack_config = require("../webpack.config")(config);

  var webpack_compiler;

  var afterBuild = function(cb) {
    return function(err, stats) {
      if(err) throw new plugins.util.PluginError('webpack', err);
      plugins.util.log('[webpack]', stats.toString({
        chunks: false
        ,colors: true
      }));
      if(typeof cb !== 'undefined') {
        cb();
      }
    };
  };

  webpack_compiler = webpack(webpack_config);

  return {
    build: function(cb){
      console.log(chalk.green("\n ..............Building Scripts.............."));

      webpack_compiler.run(afterBuild(cb));
    }
  }

};
